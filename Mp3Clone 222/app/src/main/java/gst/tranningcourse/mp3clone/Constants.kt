package gst.tranningcourse.mp3clone

object Constants {
    const val CHANNEL_ID = "MusicPlayG4"
    const val ACTION_PAUSE = 1
    const val ACTION_RESUME = 2
    const val ACTION_BACK = 3
    const val ACTION_NEXT = 4
    const val ACTION_START = 5
    const val ACTION_CLEAR = 6
}