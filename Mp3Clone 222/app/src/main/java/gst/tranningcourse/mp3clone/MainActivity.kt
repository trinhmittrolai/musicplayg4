package gst.tranningcourse.mp3clone

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import gst.tranningcourse.mp3clone.fragment.CategoryFragment

class MainActivity : AppCompatActivity() , IClickItemMusic{
    private var listMusic = mutableListOf<Song>()
    private var recyclerView: RecyclerView? = null
    private var adapter: MusicAdapter? = null
    private var imgSong: ImageView? = null
    private var imgBack: ImageView? = null
    private var imgPause: ImageView? = null
    private var imgNext: ImageView? = null
    private var tvTittle: TextView? = null
    private var tvSinger: TextView? = null
    private var mSong: Song?=null
    private var isPlaying:Boolean? = null
    private var actionMusic:Int? = null
    private var position = 0
    private var bottomNavigationView:BottomNavigationView? = null
    private var viewPager:ViewPager? = null
    private var keyMain =""

    private lateinit var layoutBottom: RelativeLayout

    //lấy dữ liệu từ Musicservice
    private var broadcastReceiver = object : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            val bundle:Bundle? = p1?.extras
            if (bundle != null) {
                mSong = bundle.get("object_song") as Song
                isPlaying = bundle.getBoolean("status_player")
                actionMusic = bundle.getInt("action_music")
                position = bundle.getInt("position")
                handleLayoutMusic(actionMusic!!)
                sendSong() //gửi action thông tin bài hát mới cho PlayActivity để cập nhật layout
            }
        }
    }

    private fun sendSong(){
        val intent = Intent("sendSong")
        val bundle = Bundle()
        bundle.putSerializable("Song", mSong)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter("send_data_to_activity"))
        init()
        setLayoutBottom()
        setUpViewPager()
        layoutBottom.setOnClickListener {
            startPlayActivity()
        }
//        adapter?.setOnClickItemMusic(this)
    }

    private fun startPlayActivity(){
        val intent = Intent(this,PlayActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("Song", mSong)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun setLayoutBottom() {
        bottomNavigationView?.setOnNavigationItemReselectedListener {
            when (it.itemId) {
                R.id.action_home -> {
                    viewPager?.currentItem = 0
                }
                R.id.action_rank -> viewPager?.currentItem = 1
                R.id.action_account -> viewPager?.currentItem = 2
            }
        }

    }

    private fun handleLayoutMusic(actionMusic: Int) {
        when(actionMusic){
            Constants.ACTION_START ->{
                layoutBottom.visibility = View.VISIBLE
                showInfoSong()
                setStatusPlayorPause()
            }
            Constants.ACTION_PAUSE ->{
                setStatusPlayorPause()
            }
            Constants.ACTION_RESUME -> setStatusPlayorPause()
            Constants.ACTION_NEXT ->{
                setStatusNextorBack()
                showInfoSong()
            }
            Constants.ACTION_BACK -> {
                setStatusNextorBack()
                showInfoSong()

            }

        }
    }

    private fun setStatusNextorBack() {
        when(actionMusic){
            Constants.ACTION_NEXT ->{
                if (position+1 >= listMusic.size) {
                    position = 0
                }
            }
            Constants.ACTION_BACK ->{
                if (position -1 < 0) {
                    position = listMusic.size - 1
                }
            }

        }
        mSong = listMusic[position]
    }
    private fun showInfoSong(){
        tvTittle?.text = mSong?.tittle
        tvSinger?.text = mSong?.singer
        Picasso.get().load(mSong?.imageUrl).into(imgSong)

    }
    private fun clickImg(){

        imgPause?.setOnClickListener {
            if(isPlaying!!){
                sendActionToService(Constants.ACTION_PAUSE)
            }
            else{
                sendActionToService(Constants.ACTION_RESUME)
            }
        }
        imgNext?.setOnClickListener {
            imgPause?.setImageResource(R.drawable.pause)
            sendActionToService(Constants.ACTION_NEXT)
        }
        imgBack?.setOnClickListener {
            imgPause?.setImageResource(R.drawable.pause)
            sendActionToService(Constants.ACTION_BACK)

        }
    }

    private fun setStatusPlayorPause(){
        clickImg()
        if(isPlaying!!){
            imgPause?.setImageResource(R.drawable.pause)
        }
        else imgPause?.setImageResource(R.drawable.play)

    }

    private fun sendActionToService(action: Int){
        Log.d("KieuTrinh", listMusic.size.toString())
        val intent = Intent(this, MusicService::class.java)
        intent.putExtra("action_receiver",action)
        startService(intent)

    }



    //ánh xạ view
    private fun init() {
        imgSong = findViewById(R.id.img_song_main)
        imgBack = findViewById(R.id.img_back)
        imgPause = findViewById(R.id.img_pause)
        imgNext = findViewById(R.id.img_next)
        tvTittle = findViewById(R.id.tv_title_song_main)
        tvSinger = findViewById(R.id.tv_single_song_main)
        //recyclerView = findViewById(R.id.recyclerView)
        layoutBottom = findViewById(R.id.layoutBottom)
        bottomNavigationView = findViewById(R.id.bottom_navi)
        viewPager = findViewById(R.id.viewpager)
        adapter = MusicAdapter()
        adapter?.setOnClickItemMusic(this)
        //recyclerView?.adapter = adapter
        //getData()
    }

    private fun setUpViewPager() {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPager?.adapter = viewPagerAdapter
        viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when(position){
                    0 -> bottomNavigationView?.menu?.findItem(R.id.action_home)?.isChecked = true
                    1 -> bottomNavigationView?.menu?.findItem(R.id.action_rank)?.isChecked = true
                    2 -> bottomNavigationView?.menu?.findItem(R.id.action_account)?.isChecked = true
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }

        })
    }

    //Lấy all data từ firebase
     fun getData(){
        val db = Firebase.database.reference
        db.child("category").child("vn_song").addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                //for (ds in snapshot.children) {
                    val song= snapshot.getValue(Song::class.java)
                    if (song != null) {
                        listMusic.add(song)
                    }

               // }
                adapter?.updateDatas(listMusic)
                //Log.d("KieuTrinh", "onChildAdded: ")
            }


            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("KieuTrinh", "onChildChanged: ")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                Log.d("KieuTrinh", "onChildRemoved: ")

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("KieuTrinh", "onChildMoved: ")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("KieuTrinh", "onCancelled: ")
            }

        })

    }


    //truyền data sang Musicservice
    override fun onClickItem(song: Song,list: MutableList<Song>) {
        listMusic = list
        val intent = Intent(this, MusicService::class.java)
        val bundel = Bundle()
        bundel.putSerializable("song",song)
        bundel.putParcelableArrayList("listSong",ArrayList(list))
        Log.d("listmusic", "onClickItem: "+listMusic.size)
        intent.putExtras(bundel)
        startService(intent)
    }
    // tắt đăng kí broadcastReceiver khi thoát app
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }



    fun gotoFragment(key:String){
        keyMain = key
        //viewPager?.currentItem = 3
    }


    fun getKey():String{
        return keyMain
    }
}