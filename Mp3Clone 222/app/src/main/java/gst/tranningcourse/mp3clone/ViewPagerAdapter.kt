package gst.tranningcourse.mp3clone

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import gst.tranningcourse.mp3clone.fragment.*

class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT ) {
    override fun getCount(): Int = 4

    override fun getItem(position: Int): Fragment {
        when(position){
            0 -> return HomeFragment()
            1-> return RankFragment()
            2 -> return AccountFragment()
            3 -> return CategoryFragment()
        }
        return HomeFragment()
    }
}