package gst.tranningcourse.mp3clone

import android.annotation.SuppressLint
import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.RemoteViews
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.net.toUri
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.squareup.picasso.Picasso
import gst.tranningcourse.mp3clone.Constants.ACTION_BACK
import gst.tranningcourse.mp3clone.Constants.ACTION_CLEAR
import gst.tranningcourse.mp3clone.Constants.ACTION_NEXT
import gst.tranningcourse.mp3clone.Constants.ACTION_PAUSE
import gst.tranningcourse.mp3clone.Constants.ACTION_RESUME
import gst.tranningcourse.mp3clone.Constants.ACTION_START
import gst.tranningcourse.mp3clone.Constants.CHANNEL_ID



class MusicService: Service() {
    private var media:MediaPlayer? = null
    private var isPlaying = false
    private lateinit var mSong:Song
    private var listMusic = mutableListOf<Song>()
    private var position = 0
    private var handle = Handler() //Khai báo một biến handle để thực hiện delay thời gian thực hiện funtion

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("Kieu Trinh", "onCreate: ")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val bundel = intent?.extras

        if(bundel!=null){
            val song = bundel.getSerializable("song") as? Song
            if(bundel.getParcelableArrayList<Song>("listSong")!=null){
                listMusic = (bundel.getParcelableArrayList<Song>("listSong") as MutableList<Song>)
            }

            if (song != null) {
                mSong = song
                position = song.mediaId.toInt()-1
                startMusic(song)
                sendNotification(song)
                sendTime() // thực hiện gửi data thời gian hiện tại của bài hát cho PlayActivity
            }
        }

        val actionMusic = intent?.getIntExtra("action_receiver",0)
        if (actionMusic != null) {
            handleActionMusic(actionMusic)
        }
        for(i in position until listMusic.size){
            media?.setOnCompletionListener {
                nextMusic()
                sendActionToActivity(ACTION_NEXT)
            }
        }

        return START_NOT_STICKY
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun sendNotification(song: Song) {
        val intent2 = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this,0,intent2, PendingIntent.FLAG_UPDATE_CURRENT)
        var bm:Bitmap?=null
        Picasso.get().load(song.imageUrl).into(object : com.squareup.picasso.Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                // loaded bitmap is here (bitmap)
                bm = bitmap
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
        })
        val remoteViews = RemoteViews(packageName, R.layout.item_music)
        remoteViews.setTextViewText(R.id.tv_title_song,song.tittle)
        remoteViews.setTextViewText(R.id.tv_single_song,song.singer)
        remoteViews.setImageViewBitmap(R.id.img_song,bm)
        remoteViews.setImageViewResource(R.id.back,R.drawable.back)
        remoteViews.setImageViewResource(R.id.next,R.drawable.next)
        remoteViews.setImageViewResource(R.id.pause,R.drawable.pause)
        remoteViews.setImageViewResource(R.id.clear,R.drawable.close)

        if(isPlaying){
            remoteViews.setOnClickPendingIntent(R.id.pause,getPendingIntent(this, ACTION_PAUSE))
            remoteViews.setImageViewResource(R.id.pause,R.drawable.pause)

        }else{
            remoteViews.setOnClickPendingIntent(R.id.pause,getPendingIntent(this, ACTION_RESUME))
            remoteViews.setImageViewResource(R.id.pause,R.drawable.play)
        }

        remoteViews.setOnClickPendingIntent(R.id.clear,getPendingIntent(this, ACTION_CLEAR))
        remoteViews.setOnClickPendingIntent(R.id.next,getPendingIntent(this, ACTION_NEXT))
        remoteViews.setOnClickPendingIntent(R.id.back,getPendingIntent(this, ACTION_BACK))


        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_baseline_music_video_24)
            .setContentIntent(pendingIntent)
            .setCustomContentView(remoteViews)
            .setSound(null)
            .build()
        startForeground(1,notification)

    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun getPendingIntent(context:Context, action: Int):PendingIntent{
        val intent = Intent(this,MusicReceiver::class.java)
        intent.putExtra("action_music",action)
        return PendingIntent.getBroadcast(context.applicationContext,action,intent,PendingIntent.FLAG_CANCEL_CURRENT)
    }

    private fun startMusic(song: Song) {
        if(media == null){
            media = MediaPlayer.create(applicationContext,song.songUrl.toUri())
            media?.start()
            isPlaying = true
        }
        else if(media != null && isPlaying){
            media?.stop()
            media = MediaPlayer.create(applicationContext,song.songUrl.toUri())
            media?.start()
        }
        sendDataToPlay()
        sendActionToActivity(ACTION_START)
    }

    private fun sendDataToPlay() {

    }

    private fun handleActionMusic(action:Int){
        when(action){
            ACTION_PAUSE -> pauseMusic()
            ACTION_RESUME -> resumeMusic()
            ACTION_NEXT -> nextMusic()
            ACTION_BACK -> backMusic()
            ACTION_CLEAR-> closeMusic()
        }
    }

    private fun closeMusic() {
        stopSelf()

    }

    private fun backMusic() {
        position--
        if (position < 0) {
            position = listMusic.size - 1
        }
        startMusic(listMusic[position])
        sendNotification(listMusic[position])
        sendActionToActivity(ACTION_BACK)

    }

    private fun nextMusic() {
        position++
        if (position >= listMusic.size) {
            position = 0
        }

        isPlaying = true
        startMusic(listMusic[position])
        sendNotification(listMusic[position])
        sendActionToActivity(ACTION_NEXT)
    }

    private fun resumeMusic() {
        if(media!=null && !isPlaying){
            media?.start()
            isPlaying = true
            mSong = listMusic[position]
            sendNotification(mSong)
            sendActionToActivity(ACTION_RESUME)
        }
    }

    private fun pauseMusic() {
        if(media!=null && isPlaying){
            media?.pause()
            isPlaying = false
            mSong = listMusic[position]
            sendNotification(mSong)
            sendActionToActivity(ACTION_PAUSE)
        }
    }

    override fun onDestroy() {
        Log.d("Kieu Trinh", "onDestroy: ")
        super.onDestroy()
        if(media != null){
            media!!.release()
            media = null
        }
    }

    private fun sendActionToActivity(action: Int){
        val intent = Intent("send_data_to_activity")
        val bundle = Bundle()
        bundle.putSerializable("object_song",mSong)
        bundle.putBoolean("status_player",isPlaying)
        bundle.putInt("action_music",action)
        bundle.putInt("position",position)
        intent.putExtras(bundle)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /*
    gửi một action updateSeekbarAndTime để gửi đi dữ liệu data của bài hát
     */
    private fun sendTime(){
        val intent = Intent("updateSeekbarAndTime")
        val bundle = Bundle()
        bundle.putInt("Time", media?.currentPosition!!)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        // runnable thực hiện lặp tại fun sendTime để gửi data thời gian hiện tại của bài hát liên tục
        val runnable = Runnable {
            sendTime()
        }
        //handle thực hiện delay 1s mới gửi data thời gian bà hát 1 lần
        handle.postDelayed(runnable, 1000)
    }
}