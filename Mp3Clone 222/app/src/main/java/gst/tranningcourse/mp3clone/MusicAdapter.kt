package gst.tranningcourse.mp3clone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class MusicAdapter : RecyclerView.Adapter<MusicAdapter.MusicViewHolder>() {
    private val listMusic = mutableListOf<Song>()
    private var iClickedItem:IClickItemMusic? = null
    fun setOnClickItemMusic(iClickedItem: IClickItemMusic) {
        this.iClickedItem = iClickedItem
    }

    class MusicViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val txtTitle: TextView = itemView.findViewById(R.id.tv_title_song_home)
        val txtSingle: TextView = itemView.findViewById(R.id.tv_single_song_home)
        val imageMusic: ImageView = itemView.findViewById(R.id.img_song_foryou)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_recyclerview_foryou,parent,false)
        return MusicViewHolder(view)
    }

    override fun onBindViewHolder(holder: MusicViewHolder, position: Int) {
        val song:Song = listMusic[position]
        holder.apply {
            txtTitle.text = listMusic[position].tittle
            txtSingle.text = listMusic[position].singer
            Picasso.get().load(listMusic[position].imageUrl).into(imageMusic)
        }
        holder.itemView.setOnClickListener {
            iClickedItem?.onClickItem(song,listMusic)
        }
    }

    override fun getItemCount(): Int {
        return listMusic.size
    }
    fun updateDatas(newDatas : List<Song>){
        listMusic.clear()
        listMusic.addAll(newDatas)
        notifyDataSetChanged()
    }
}