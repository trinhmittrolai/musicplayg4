package gst.tranningcourse.mp3clone.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import gst.tranningcourse.mp3clone.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategoryFragment : Fragment() ,IClickItemMusic{
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var rvCategory:RecyclerView? = null
    private var keyCategoty:String = ""
    private var tv_category:TextView? = null
    private var mainActivity:MainActivity?=null
    private var adapter:MusicAdapter? = null
    private var listMusic = mutableListOf<Song>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_category, container, false)
        init(view)
        return view
    }

    private fun init(view: View) {
        mainActivity = activity as MainActivity
        tv_category = view.findViewById(R.id.tv_category_ez)
        keyCategoty = mainActivity!!.getKey()
        tv_category?.setText(keyCategoty)
        rvCategory = view.findViewById(R.id.rv_category)
        adapter = MusicAdapter()
        rvCategory?.addItemDecoration(
            DividerItemDecoration(
                context, LinearLayout.VERTICAL
            )
        )
        rvCategory?.layoutManager = LinearLayoutManager(context)
        rvCategory?.adapter = adapter
        if(keyCategoty.equals("category")){
            getData2()
        }
        else if(keyCategoty.equals("top100")){
            getData("category","jp_song")
            getData2()
        }
        else{
            getData("category",keyCategoty)
        }
        adapter?.setOnClickItemMusic(this)

    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CategoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
    private fun getData(key1:String,key2:String){
        val db = Firebase.database.reference
        db.child(key1).child(key2).addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                //for (ds in snapshot.children) {
                val song= snapshot.getValue(Song::class.java)
                if (song != null) {
                    listMusic.add(song)
                }

                // }
                adapter?.updateDatas(listMusic)
            }


            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("KieuTrinh", "onChildChanged: ")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                Log.d("KieuTrinh", "onChildRemoved: ")

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("KieuTrinh", "onChildMoved: ")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("KieuTrinh", "onCancelled: ")
            }

        })

    }
    private fun getData2() {
        val db = Firebase.database.reference
        db.child("category").addChildEventListener(object : ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                for (ds in snapshot.children) {
                    val song= ds.getValue(Song::class.java)
                    if (song != null) {
                        listMusic.add(song)
                    }
                }
                adapter?.updateDatas(listMusic)
            }


            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })

    }


    override fun onClickItem(song: Song,list: MutableList<Song>) {
        mainActivity?.onClickItem(song,list)
    }

}