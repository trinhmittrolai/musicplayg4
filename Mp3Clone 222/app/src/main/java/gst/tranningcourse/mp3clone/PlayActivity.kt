package gst.tranningcourse.mp3clone

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class PlayActivity : AppCompatActivity(),Runnable{

    private var tvTittlePlayer: TextView?=null
    private var tvSingerPlayer: TextView?=null
    private var imgSongPlayer: CircleImageView?=null
    private var btnImgBackPlayer: ImageButton?=null
    private var btnImgNextPlayer: ImageButton?=null
    private var btnImgPausePlayer: ImageButton?=null
    private var sbPlayer: SeekBar? = null
    private var tvTimeProces:TextView? = null
    private var tvTimePlayer:TextView? = null
    private var mSong:Song? = null
    private var time: Int = 0

    /*
    Để nhận thời gian hiện tại của bài hát tại Class MusicService thêm funtion sendTime() trong onStartCommand
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_music)

        //khai báo nhận action để cập nhật thời gian của bài hát
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter("updateSeekbarAndTime"))
        //Khai báo nhận action thay đổi bài hát để nhận thông tin bài hát mới
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter("sendSong"))

        getData()
        iNit()
        setLayout()
        startAnimation()
    }

    private fun getData() {
        val bundel = intent?.extras
        if(bundel!=null) {
            mSong = bundel.getSerializable("Song") as? Song
        }
    }

    //sau khi nhận thời gian của bài hát thì thực hiện cập nhật seekbar và thời gian hiển thị
    private fun updateSeekBarAndTime(currPos: Int) {
        sbPlayer?.progress = currPos
        tvTimeProces?.text = setTime(currPos)
    }

    private fun setLayout() {
        tvTittlePlayer?.text = mSong?.tittle
        tvSingerPlayer?.text = mSong?.singer
        Picasso.get().load(mSong?.imageUrl).into(imgSongPlayer)
        time = MediaPlayer.create(applicationContext, mSong?.songUrl?.toUri()).duration
        tvTimePlayer?.text = setTime(time)
        sbPlayer?.max = time
    }

    private fun iNit() {
        tvTittlePlayer = findViewById(R.id.tv_tittle_player)
        tvSingerPlayer = findViewById(R.id.tv_singer_player)
        imgSongPlayer = findViewById(R.id.img_song_player)
        btnImgBackPlayer = findViewById(R.id.btn_img_back_player)
        btnImgPausePlayer = findViewById(R.id.btn_img_play_or_pause_player)
        btnImgNextPlayer = findViewById(R.id.btn_img_next_player)
        sbPlayer = findViewById(R.id.sb_player)
        tvTimeProces = findViewById(R.id.tv_time_progress)
        tvTimePlayer = findViewById(R.id.tv_time_player)
    }


    private fun startAnimation(){
        val runnable = Runnable{
            run()
        }
        imgSongPlayer?.animate()?.rotationBy(360F)?.withEndAction(runnable)?.setDuration(10000)
            ?.setInterpolator(LinearInterpolator())?.start()
    }
    override fun run() {
        imgSongPlayer?.animate()?.rotationBy(360F)?.withEndAction(this)?.setDuration(10000)
            ?.setInterpolator(LinearInterpolator())?.start()
    }

    private fun setTime(time: Int): String{
        val second = (time / 1000) % 60
        val minute = time / 60000
        return if (second < 10) "$minute:0$second" else "$minute:$second"
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    //nhận action và data từ intent
    val broadcastReceiver = object : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1?.action?.equals("updateSeekbarAndTime")!!){
                Log.e("Hoang", "updateSeekbarAndTime")
                val bundle = p1.extras
                if (bundle != null){
                    updateSeekBarAndTime(bundle.getInt("Time"))
                }
            }else{
                if (p1.action?.equals("sendSong")!!){
                    Log.e("Hoang", "sendSong")
                    val bundle = p1.extras
                    if (bundle != null){
                        mSong = bundle.get("Song") as Song
                        Log.e("Hoang", "${mSong!!.tittle}")
                        setLayout()
                    }
                }
            }
        }
    }
}