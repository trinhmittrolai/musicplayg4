package gst.tranningcourse.mp3clone

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class Song(
    val mediaId:String="",
    val tittle:String="",
    val singer:String="",
    val songUrl:String="",
    val imageUrl:String=""
):Serializable, Parcelable {
    constructor():this("","","","","")
}
