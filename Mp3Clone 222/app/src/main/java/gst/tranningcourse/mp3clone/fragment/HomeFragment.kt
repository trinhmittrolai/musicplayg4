package gst.tranningcourse.mp3clone.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import gst.tranningcourse.mp3clone.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() ,View.OnClickListener,IClickItemMusic{
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var tvNewMusic:TextView? = null
    private var tvCategory:TextView? = null
    private var tvTopMusic:TextView? = null
    private var tvVip:TextView? = null
    private var tvEvent:TextView? = null
    private var imgSearchHome:ImageView? = null
    private var autoCompletetvHome:AutoCompleteTextView? = null
    private var key:String? = null
    private var listMusic = mutableListOf<Song>()
    private var mainActivity:MainActivity? = null
    private var mISendKey:ISendKeyToFragment? = null
    private var rvForYou:RecyclerView? = null
    private var adapter: MusicAdapter? = null

    /*
    trong thư mục settings.gardle thêm maven { url  'https://jitpack.io' } trong
    repositories {
        google()
        mavenCentral()
        jcenter() // Warning: this repository is going to shut down soon
        maven { url  'https://jitpack.io' }
    }

    trong thư mục build.gradle implemen thu viện:
    implementation 'com.github.denzcoskun:ImageSlideshow:0.1.0'

    trong layout activity home sửa viewPage của slide ảnh bằng SlideImage mà tôi thêm vào
     */
    private var imageSlider: ImageSlider? = null //khai báo một view của sliderImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View? = inflater.inflate(R.layout.fragment_home, container, false)
        // Inflate the layout for this fragment

        if (view != null) {
            iNit(view)
        }
        return view
    }

    private fun iNit(view: View) {
        mainActivity = activity as MainActivity
        tvNewMusic = view.findViewById(R.id.text_view_new_music_home)
        tvCategory = view.findViewById(R.id.text_view_category_home)
        tvTopMusic = view.findViewById(R.id.text_view_top_100_home)
        tvVip = view.findViewById(R.id.text_view_vip_home)
        tvEvent = view.findViewById(R.id.text_view_event_home)
        autoCompletetvHome = view.findViewById(R.id.auto_complete_text_view_home)
        imgSearchHome = view.findViewById(R.id.img_search_home)
        rvForYou = view.findViewById(R.id.recycler_for_you_home)
        adapter = MusicAdapter()
        imageSlider = view.findViewById(R.id.image_slider_home) //ánh xạ view

        rvForYou?.addItemDecoration(
            DividerItemDecoration(
                context,LinearLayout.VERTICAL
            )
        )
        rvForYou?.layoutManager = LinearLayoutManager(context)
        rvForYou?.adapter = adapter
        getData()

        setImageViewSlider() //set data ảnh vào cho sileImage

        adapter?.setOnClickItemMusic(this)
        tvNewMusic?.setOnClickListener(this)
        tvCategory?.setOnClickListener(this)
        tvTopMusic?.setOnClickListener(this)
        tvVip?.setOnClickListener(this)
        tvEvent?.setOnClickListener(this)
        autoCompletetvHome?.setOnClickListener(this)

        //Log.d("KieuTrinh", listMusic.size.toString()+" home")

    }

    //tạo một ArrayList(SliderModel()) và set list cho imageSlider
    private fun setImageViewSlider() {
        val imageList = ArrayList<SlideModel>()
        imageList.add(SlideModel(R.drawable.slide1))
        imageList.add(SlideModel(R.drawable.slide2))
        imageList.add(SlideModel(R.drawable.slide3))

        imageSlider?.setImageList(imageList)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(p0: View?) {
        key = null
        when(p0?.id){
            R.id.text_view_new_music_home -> {
                key = "Nhạc mới"
                mainActivity?.gotoFragment(key!!)
            }
            R.id.text_view_category_home -> {
                key = "category"
                mainActivity?.gotoFragment(key!!)
            }
            R.id.text_view_top_100_home -> {
                key = "top100"
                mainActivity?.gotoFragment(key!!)
            }
            R.id.img_search_home ->{
                if(autoCompletetvHome?.text.toString().equals("")){
                    Toast.makeText(context,"Không được để trống",Toast.LENGTH_SHORT).show()
                }
                else{
                    key = autoCompletetvHome?.text.toString()
                    mainActivity?.gotoFragment(key!!)
                }
            }
        }
    }

    private fun getData() {
        val db = Firebase.database.reference
        db.child("category").addChildEventListener(object : ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                for (ds in snapshot.children) {
                    val song= ds.getValue(Song::class.java)
                    if (song != null) {
                        listMusic.add(song)
                    }
                }
               if(listMusic.size<=25){
                   adapter?.updateDatas(listMusic)
               }
            }


            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })
    }

    override fun onClickItem(song: Song,list:MutableList<Song>) {
        mainActivity?.onClickItem(song,list)
    }
}