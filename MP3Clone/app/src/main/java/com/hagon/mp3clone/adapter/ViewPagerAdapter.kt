package com.hagon.mp3clone.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.hagon.mp3clone.fragment.LibraryFragment
import com.hagon.mp3clone.fragment.HomeFragment
import com.hagon.mp3clone.fragment.RankFragment
import com.hagon.mp3clone.fragment.SearchFragment

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return 4
    }

    override fun createFragment(position: Int): Fragment {
       return when(position){
            0 -> HomeFragment()
            1 -> SearchFragment()
            2 -> RankFragment()
            3 -> LibraryFragment()
            else -> Fragment()
        }
    }

}