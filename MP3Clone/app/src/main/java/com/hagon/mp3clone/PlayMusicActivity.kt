package com.hagon.mp3clone

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.hagon.mp3clone.model.Song
import de.hdodenhof.circleimageview.CircleImageView

class PlayMusicActivity: AppCompatActivity(), Runnable {

    private var imgAvatar: CircleImageView? = null
    private var imgBackMusic: ImageView? = null
    private var imgPlayOrPause: ImageView? = null
    private var imgNextMusic: ImageView? = null
    private var tvNameSong: TextView? = null
    private var tvSingle: TextView? = null
    private var tvTime: TextView? = null
    private var tvTimeProgress: TextView? = null
    private var sbPlayMusic: SeekBar? = null
    private var isPlaying: Boolean = true
    private var time: Int = 0
    private var handle = Handler()

    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_music)

        init()

        val bundle = intent.extras
        if (bundle != null){
            val song = bundle.getSerializable("Song") as Song
            setData(song)
            mediaPlayer?.start()
        }else{
            Toast.makeText(this, "No data", Toast.LENGTH_SHORT).show()
        }

        startAnimation()

        mediaPlayer?.setOnPreparedListener(MediaPlayer.OnPreparedListener(){
            updateSeekBarAndTime()
        })

        imgPlayOrPause?.setOnClickListener { playOrPause() }
        imgNextMusic?.setOnClickListener { nextMusic() }
        imgBackMusic?.setOnClickListener { backMusic() }
    }

    private fun init(){
        imgAvatar = findViewById(R.id.img_avatar)
        imgBackMusic = findViewById(R.id.img_back_music)
        imgPlayOrPause = findViewById(R.id.img_play_or_pause_music)
        imgNextMusic = findViewById(R.id.img_next_music)
        tvNameSong = findViewById(R.id.tv_name_music)
        tvSingle = findViewById(R.id.tv_single)
        sbPlayMusic = findViewById(R.id.sb_music)
        tvTimeProgress = findViewById(R.id.tv_time_progress)
        tvTime = findViewById(R.id.tv_time)
    }

    fun setData(song: Song){
        imgAvatar?.setImageResource(song.imageUrl.toInt())
        tvNameSong?.text = song.tittle
        tvSingle?.text = song.singer
        mediaPlayer = MediaPlayer.create(applicationContext, song.songUrl.toUri())
        time = MediaPlayer.create(applicationContext, song.songUrl.toUri()).duration
        tvTime?.text = setTime(time)
        sbPlayMusic?.max = time
    }

    private fun updateSeekBarAndTime() {
        var currPos: Int = mediaPlayer?.currentPosition!!
        sbPlayMusic?.progress = currPos
        tvTimeProgress?.text = setTime(currPos)

        val runnable = Runnable {
            updateSeekBarAndTime()
        }
        handle.postDelayed(runnable, 1000)
    }

    private fun backMusic() {
        //TODO
    }

    private fun nextMusic() {
        //TODO
    }

    private fun playOrPause() {
        if (isPlaying){
            pauseMusic()
        }else{
            startMusic()
        }
    }

    private fun startMusic() {
        startAnimation()
        mediaPlayer?.start()
        imgPlayOrPause?.setImageResource(R.drawable.ic_pause_music)
        isPlaying = true
    }

    private fun pauseMusic() {
        imgAvatar?.animate()?.cancel()
        mediaPlayer?.pause()
        imgPlayOrPause?.setImageResource(R.drawable.ic_play_music)
        isPlaying = false
    }

    private fun startAnimation(){
        val runnable = Runnable{
            run()
        }
        imgAvatar?.animate()?.rotationBy(360F)?.withEndAction(runnable)?.setDuration(10000)
            ?.setInterpolator(LinearInterpolator())?.start()
    }

    override fun run() {
        imgAvatar?.animate()?.rotationBy(360F)?.withEndAction(this)?.setDuration(10000)
            ?.setInterpolator(LinearInterpolator())?.start()
    }

    private fun setTime(time: Int): String{
        val second = (time / 1000) % 60
        val minute = time / 60000
        return if (second < 10) "$minute:0$second" else "$minute:$second"
    }
}