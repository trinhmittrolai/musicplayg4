package com.hagon.mp3clone.model

import java.io.Serializable

data class Song(
    val mediaId:String="",
    val tittle:String="",
    val singer:String="",
    val songUrl:String="",
    val imageUrl:String=""
): Serializable {
    constructor():this("","","","","")
}