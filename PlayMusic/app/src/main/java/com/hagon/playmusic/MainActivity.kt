package com.hagon.playmusic

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.hagon.playmusic.model.Song

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val song = Song(1.toString(), "Lovely",
            "Billie Eilish",
            "https://data37.chiasenhac.com/downloads/1911/5/1910939-a80b6112/128/Lovely%20-%20Billie%20Eilish_%20Khalid.mp3",
            R.drawable.img_avatar.toString())

        val btnStart: Button = findViewById(R.id.btn_start)
        btnStart.setOnClickListener {
            val intent = Intent(this, PlayMusicActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable("Song", song)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
}