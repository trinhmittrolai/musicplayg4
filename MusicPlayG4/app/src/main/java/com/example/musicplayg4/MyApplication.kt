package com.example.musicplayg4

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.example.musicplayg4.Constants.CHANNEL_ID

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        createChannelNotification()
    }

    private fun createChannelNotification() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val chanel = NotificationChannel(CHANNEL_ID,"Channel service TrinhKV",
                NotificationManager.IMPORTANCE_DEFAULT)
            chanel.setSound(null,null)
            val manager: NotificationManager = getSystemService(NotificationManager::class.java) as NotificationManager
            manager.createNotificationChannel(chanel)
        }
    }
}