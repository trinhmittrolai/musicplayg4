package com.example.musicplayg4

import android.annotation.SuppressLint
import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.Bundle
import android.os.IBinder
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.net.toUri
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.musicplayg4.Constants.ACTION_BACK
import com.example.musicplayg4.Constants.ACTION_NEXT
import com.example.musicplayg4.Constants.ACTION_PAUSE
import com.example.musicplayg4.Constants.ACTION_RESUME
import com.example.musicplayg4.Constants.ACTION_START
import com.example.musicplayg4.Constants.CHANNEL_ID
import com.squareup.picasso.Picasso

class MusicService: Service() {
    private var media:MediaPlayer? = null
    private var isPlaying = false
    private lateinit var mSong:Song

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val bundel = intent?.extras
        if(bundel!=null){
            val song = bundel.getSerializable("song") as? Song
            if (song != null) {
                mSong = song
                startMusic(song)
                sendNotification(song)
            }

        }

        val actionMusic = intent?.getIntExtra("action_receiver",0)
        if (actionMusic != null) {
            handleActionMusic(actionMusic)
        }
        return START_NOT_STICKY
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun sendNotification(song: Song) {
        val intent2 = Intent(this,MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this,0,intent2, PendingIntent.FLAG_UPDATE_CURRENT)
        var bm:Bitmap?=null
        Picasso.get().load(song.imageUrl).into(object : com.squareup.picasso.Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                // loaded bitmap is here (bitmap)
                bm = bitmap
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
        })
        val remoteViews = RemoteViews(packageName,R.layout.item_music)
        remoteViews.setTextViewText(R.id.tv_title_song,song.tittle)
        remoteViews.setTextViewText(R.id.tv_single_song,song.singer)
        remoteViews.setImageViewBitmap(R.id.img_song,bm)
        remoteViews.setImageViewResource(R.id.back,R.drawable.back)
        remoteViews.setImageViewResource(R.id.next,R.drawable.next)
        remoteViews.setImageViewResource(R.id.pause,R.drawable.pause)

        if(isPlaying){
            remoteViews.setOnClickPendingIntent(R.id.pause,getPendingIntent(this, ACTION_PAUSE))
            remoteViews.setImageViewResource(R.id.pause,R.drawable.pause)
            remoteViews.setOnClickPendingIntent(R.id.next,getPendingIntent(this, ACTION_NEXT))
        }else{
            remoteViews.setOnClickPendingIntent(R.id.pause,getPendingIntent(this, ACTION_RESUME))
            remoteViews.setImageViewResource(R.id.pause,R.drawable.play)
        }


        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentIntent(pendingIntent)
            .setCustomContentView(remoteViews)
            .setSound(null)
            .build()
        startForeground(1,notification)

    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun getPendingIntent(context:Context, action: Int):PendingIntent{
        val intent = Intent(this,MusicReceiver::class.java)
        intent.putExtra("action_music",action)
        return PendingIntent.getBroadcast(context.applicationContext,action,intent,PendingIntent.FLAG_CANCEL_CURRENT)
    }

    private fun startMusic(song: Song) {
        if(media == null){
            media = MediaPlayer.create(applicationContext,song.songUrl.toUri())
            media?.start()
            isPlaying = true
        }
        else if(media != null && isPlaying){
            media?.stop()
            media = MediaPlayer.create(applicationContext,song.songUrl.toUri())
            media?.start()
        }
        sendActionToActivity(ACTION_START)
    }

    private fun handleActionMusic(action:Int){
        when(action){
            ACTION_PAUSE -> pauseMusic()
            ACTION_RESUME -> resumeMusic()
            ACTION_NEXT -> nextMusic()
            ACTION_BACK -> backMusic()
        }
    }

    private fun backMusic() {

    }

    private fun nextMusic() {
//        if(media!=null){
//            media?
//        }
    }

    private fun resumeMusic() {
        if(media!=null && !isPlaying){
            media?.start()
            isPlaying = true
            sendNotification(mSong)
            sendActionToActivity(ACTION_RESUME)
        }
    }

    private fun pauseMusic() {
        if(media!=null && isPlaying){
            media?.pause()
            isPlaying = false
            sendNotification(mSong)
            sendActionToActivity(ACTION_PAUSE)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(media != null){
            media!!.release()
            media = null
        }
    }

    private fun sendActionToActivity(action: Int){
        val intent = Intent("send_data_to_activity")
        val bundle = Bundle()
        bundle.putSerializable("object_song",mSong)
        bundle.putBoolean("status_player",isPlaying)
        bundle.putInt("action_music",action)
        intent.putExtras(bundle)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}