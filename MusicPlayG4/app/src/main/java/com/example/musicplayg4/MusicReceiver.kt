package com.example.musicplayg4

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class MusicReceiver:BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        var actionMusic = p1?.getIntExtra("action_music",0)
        var intentService = Intent(p0,MusicService::class.java)
        intentService.putExtra("action_receiver",actionMusic)
        p0?.startService(intentService)
    }
}