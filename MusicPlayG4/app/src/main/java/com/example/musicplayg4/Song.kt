package com.example.musicplayg4

import java.io.Serializable

data class Song(
    val mediaId:String="",
    val tittle:String="",
    val singer:String="",
    val songUrl:String="",
    val imageUrl:String=""
):Serializable{
    constructor():this("","","","","")
}
