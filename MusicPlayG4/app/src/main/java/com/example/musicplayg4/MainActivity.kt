package com.example.musicplayg4

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso


class MainActivity : AppCompatActivity() ,IClickItemMusic{
    private var listMusic = mutableListOf<Song>()
    private lateinit var media: MediaPlayer
    private var recyclerView:RecyclerView? = null
    private var adapter:MusicAdapter? = null
    private var imgSong:ImageView? = null
    private var imgBack:ImageView? = null
    private var imgPause:ImageView? = null
    private var imgNext:ImageView? = null
    private var tvTittle:TextView? = null
    private var tvSinger:TextView? = null
    private var mSong:Song?=null
    private var isPlaying:Boolean? = null
    private lateinit var layoutBottom:RelativeLayout
    private var broadcastReceiver = object : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            val bundle:Bundle? = p1?.extras
            if (bundle != null) {
                mSong = bundle.get("object_song") as Song
                isPlaying = bundle.getBoolean("status_player")
                val actionMusic = bundle.getInt("action_music")

                handleLayoutMusic(actionMusic)

            }
        }

    }

    private fun handleLayoutMusic(actionMusic: Int) {
                when(actionMusic){
                    Constants.ACTION_START ->{
                        layoutBottom.visibility = View.VISIBLE
                        showInfoSong()
                        setStatusPlayorPause()
                    }
                    Constants.ACTION_PAUSE -> setStatusPlayorPause()
                    Constants.ACTION_RESUME -> setStatusPlayorPause()
//                    Constants.ACTION_NEXT ->
//                    Constants.ACTION_BACK ->

                }
    }
    private fun showInfoSong(){
        Picasso.get().load(mSong?.imageUrl).into(imgSong)
        tvTittle?.text = mSong?.tittle.toString()
        tvSinger?.text = mSong?.singer.toString()

        imgPause?.setOnClickListener {
            if(isPlaying!!){
                sendActionToService(Constants.ACTION_PAUSE)
            }
            else{
                sendActionToService(Constants.ACTION_RESUME)
            }
        }
    }

    private fun setStatusPlayorPause(){
        if(isPlaying!!){
            imgPause?.setImageResource(R.drawable.pause)
        }
        else imgPause?.setImageResource(R.drawable.play)
    }

    private fun sendActionToService(action: Int){
        val intent = Intent(this,MusicService::class.java)
        intent.putExtra("action_receiver",action)
        startService(intent)

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter("send_data_to_activity"))
        init()
//        adapter?.setOnClickItemMusic(this)
    }

    private fun init() {
        imgSong = findViewById(R.id.img_song_main)
        imgBack = findViewById(R.id.img_back)
        imgPause = findViewById(R.id.img_pause)
        imgNext = findViewById(R.id.img_next)
        tvTittle = findViewById(R.id.tv_title_song_main)
        tvSinger = findViewById(R.id.tv_single_song_main)
        recyclerView = findViewById(R.id.recyclerView)
        layoutBottom = findViewById(R.id.layoutBottom)
        adapter = MusicAdapter()
        adapter?.setOnClickItemMusic(this)
        recyclerView?.adapter = adapter
        getData()
    }

    private fun getData() {
        val db = Firebase.database.reference
        db.child("category").addChildEventListener(object : ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                for (ds in snapshot.children) {
                    val song= ds.getValue(Song::class.java)
                    if (song != null) {
                        listMusic.add(song)
                    }
                }
                adapter?.updateDatas(listMusic)
            }


            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })

    }

    override fun onClickItem(song: Song) {
        val intent = Intent(this,MusicService::class.java)
        val bundel = Bundle()
        bundel.putSerializable("song",song)
        intent.putExtras(bundel)
        startService(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }
}